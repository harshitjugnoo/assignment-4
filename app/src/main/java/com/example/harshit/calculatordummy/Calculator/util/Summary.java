package com.example.harshit.calculatordummy.Calculator.util;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;

import com.example.harshit.calculatordummy.R;

import java.util.ArrayList;
import java.util.List;

import static com.example.harshit.calculatordummy.Calculator.main.MainActivity.map;


public class Summary extends Activity implements AppConstants {

    TextView textView;
    ListView viewSummary;
    Button clear, back;
    ArrayAdapter adapter;
    Spinner spinner;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.summary);
        textView = (TextView) findViewById(R.id.summaryTextView);
        viewSummary = (ListView) findViewById(R.id.listView);
        clear = (Button) findViewById(R.id.clearButton);
        back = (Button) findViewById(R.id.backButton);
        spinner = (Spinner) findViewById(R.id.spinner);
        viewSummary.setAdapter(adapter);
        addItemsOnSpinner();
        addListenerOnSpinnerItemSelection();
        clear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                map.get("allList").clear();
                map.get("addList").clear();
                map.get("subList").clear();
                map.get("mulList").clear();
                map.get("divList").clear();
                adapter.notifyDataSetChanged();
            }
        });
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

    }


    private void addItemsOnSpinner() {
        List<String> spinnerItems = new ArrayList<>();
        spinnerItems.add("All");
        spinnerItems.add("Addition operation");
        spinnerItems.add("Subtraction operation");
        spinnerItems.add("Multiplication operation");
        spinnerItems.add("Division operation");
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_dropdown_item, spinnerItems);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(dataAdapter);
    }

    private void addListenerOnSpinnerItemSelection() {
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position == POSITION_0) {
                    adapter = new ArrayAdapter(Summary.this, android.R.layout.simple_list_item_1, map.get("allList"));
                    adapter.notifyDataSetChanged();
                    viewSummary.setAdapter(adapter);
                } else if (position == POSITION_1) {
                    adapter = new ArrayAdapter(Summary.this, android.R.layout.simple_list_item_1, map.get("addList"));
                    adapter.notifyDataSetChanged();
                    viewSummary.setAdapter(adapter);
                } else if (position == POSITION_2) {
                    adapter = new ArrayAdapter(Summary.this, android.R.layout.simple_list_item_1, map.get("subList"));
                    adapter.notifyDataSetChanged();
                    viewSummary.setAdapter(adapter);
                } else if (position == POSITION_3) {
                    adapter = new ArrayAdapter(Summary.this, android.R.layout.simple_list_item_1, map.get("mulList"));
                    adapter.notifyDataSetChanged();
                    viewSummary.setAdapter(adapter);
                } else if (position == POSITION_4) {
                    adapter = new ArrayAdapter(Summary.this, android.R.layout.simple_list_item_1, map.get("divList"));
                    adapter.notifyDataSetChanged();
                    viewSummary.setAdapter(adapter);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                // TODO Auto-generated method stub
            }
        });
    }

}

