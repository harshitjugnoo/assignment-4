package com.example.harshit.calculatordummy.Calculator.main;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.harshit.calculatordummy.Calculator.util.Summary;
import com.example.harshit.calculatordummy.R;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;


public class MainActivity extends Activity implements View.OnClickListener {
    Button add, subtract, multiply, divide, summary, clearFields;
    EditText firstNumber, secondNumber, result;
    LinkedList<String> summaryList = new LinkedList<>(); //This list stores all the operations
    LinkedList<String> addSummaryList = new LinkedList<>(); //Individual lists are maintained
    LinkedList<String> subSummaryList = new LinkedList<>(); //for storing summary of different operations
    LinkedList<String> mulSummaryList = new LinkedList<>();
    LinkedList<String> divSummaryList = new LinkedList<>();
    public static Map<String, LinkedList> map = new HashMap<>(); //Hash Map stores the lists

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        add = (Button) findViewById(R.id.add);
        subtract = (Button) findViewById(R.id.sub);
        multiply = (Button) findViewById(R.id.mul);
        divide = (Button) findViewById(R.id.div);
        summary = (Button) findViewById(R.id.summary);
        clearFields = (Button) findViewById(R.id.clear);
        firstNumber = (EditText) findViewById(R.id.firstNumber);
        secondNumber = (EditText) findViewById(R.id.secondNumber);
        result = (EditText) findViewById(R.id.result);
        firstNumber.setHint("Enter a number");
        secondNumber.setHint("Enter a number");
        map.put("allList", summaryList);
        map.put("addList", addSummaryList);
        map.put("subList", subSummaryList);
        map.put("mulList", mulSummaryList);
        map.put("divList", divSummaryList);
        add.setOnClickListener(this);
        subtract.setOnClickListener(this);
        multiply.setOnClickListener(this);
        divide.setOnClickListener(this);
        summary.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent getSummary = new Intent(MainActivity.this, Summary.class);
                startActivity(getSummary);
            }
        });
        clearFields.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                firstNumber.setText("");
                secondNumber.setText("");
                result.setText("");
                firstNumber.requestFocus();
            }
        });

    }

    @Override
    public void onClick(View v) {

        if (firstNumber.getText().toString().equals("")) { //Notify if the first field is empty
            Toast.makeText(MainActivity.this, "Please Enter the first field!", Toast.LENGTH_SHORT).show();
            firstNumber.requestFocus(); //Cursor moves to the first number field
        } else if (secondNumber.getText().toString().equals("")) {//Notify if the second field is empty
            Toast.makeText(MainActivity.this, "Please Enter the second field!", Toast.LENGTH_SHORT).show();
            secondNumber.requestFocus(); //Cursor moves to the second number field
        } else {
            double firstNo = Double.parseDouble(firstNumber.getText().toString());
            double secondNo = Double.parseDouble(secondNumber.getText().toString());


            String res;
            double compute;
            switch (v.getId()) {
                case R.id.add:
                    compute = firstNo + secondNo;
                    result.setText(Double.toString(compute));
                    res = firstNumber.getText().toString() + " + " + secondNumber.getText().toString() + " = " + Double.toString(compute);
                    summaryList.addFirst(res);
                    addSummaryList.addFirst(res);
                    break;
                case R.id.sub:
                    compute = firstNo - secondNo;
                    result.setText(Double.toString(compute));
                    res = firstNumber.getText().toString() + " - " + secondNumber.getText().toString() + " = " + Double.toString(compute);
                    summaryList.addFirst(res);
                    subSummaryList.addFirst(res);
                    break;
                case R.id.mul:
                    compute = firstNo * secondNo;
                    result.setText(Double.toString(compute));
                    res = firstNumber.getText().toString() + " * " + secondNumber.getText().toString() + " = " + Double.toString(compute);
                    summaryList.addFirst(res);
                    mulSummaryList.addFirst(res);
                    break;
                case R.id.div:
                    if (secondNo == 0) {
                        Toast.makeText(MainActivity.this, "Divide by zero not allowed!", Toast.LENGTH_SHORT).show();
                        secondNumber.requestFocus();
                    } else {
                        compute = firstNo / secondNo;
                        result.setText(Double.toString(compute));
                        res = firstNumber.getText().toString() + " / " + secondNumber.getText().toString() + " = " + Double.toString(compute);
                        summaryList.addFirst(res);
                        divSummaryList.addFirst(res);
                    }
                    break;
            }

        }

    }


}
